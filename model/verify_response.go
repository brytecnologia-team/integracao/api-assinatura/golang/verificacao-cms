package model

import (
	"strconv"
	"strings"
)

type VerificationStatus struct {
	GeneralStatus string
	Nonce int
	SignatureFormat string
}

type CmsVerifyResponse struct {
	Nonce int
	VerificationStatus []VerificationStatus
}

func (vs VerificationStatus) Report() string {
	sb := strings.Builder{}
	sb.WriteString("\t\tVerification Status: {\n")
	sb.WriteString("\t\t\tNonce: " + strconv.Itoa(vs.Nonce) + "\n")
	sb.WriteString("\t\t\tGeneral Status: " + vs.GeneralStatus + "\n")
	sb.WriteString("\t\t\tSignature Format: " + vs.SignatureFormat + "\n")
	sb.WriteString("\t\t}\n")
	return sb.String()
}

func (cvr CmsVerifyResponse) Report() string {
	sb := strings.Builder{}
	sb.WriteString("Response Verification {\n")
	sb.WriteString("\tNonce:" + strconv.Itoa(cvr.Nonce) + "\n")
	sb.WriteString("\tVerifications: [\n")
	for _, status := range cvr.VerificationStatus {
		sb.WriteString(status.Report())
	}
	sb.WriteString("\t]\n")
	sb.WriteString("}\n")
	return sb.String()
}
