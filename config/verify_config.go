package config

const (
	AccessToken = "<ACCESS_TOKEN>"
)

//Url
const (
	UrlCmsVerification = "https://fw2.bry.com.br/api/cms-verification-service/v1/signatures/verify"
)

//Request
const (
	DocumentPath = "./resource/document.txt"
	SignatureAttachedPath = "./resource/signature-attached.cms"
	SignatureDetachedPath = "./resource/signature-detached.cms"
	SignatureNonce = "1"
	ContentReturn = "true"
)
