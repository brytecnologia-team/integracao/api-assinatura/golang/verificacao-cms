package main

import (
	"log"
	"verificacao-cms/verify"
)

func main(){
	log.Println("============ CMS Verification Request")
	verify.CmsSignatureAttachedVerify()

	log.Println("\n============ CMS Verification Request Sending Document Hash")
	verify.CmsSignatureDetachedVerifySendingHash()

	log.Println("\n============ CMS Verification Request Sending Document")
	verify.CmsSignatureDetachedVerifySendingDocument()
}
