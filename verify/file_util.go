package verify

import (
	"crypto"
	"encoding/hex"
	"io/ioutil"
	"log"
	"os"
)

func GetHexadecimalHashFile(path string) string {
	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	fileContents, err := ioutil.ReadAll(file)
	if err != nil {
		log.Fatal(err)
	}
	h := crypto.SHA256.New()
	h.Write(fileContents)
	return hex.EncodeToString(h.Sum(nil))
}
