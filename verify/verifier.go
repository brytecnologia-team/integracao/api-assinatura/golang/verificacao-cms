package verify

import (
	"fmt"
	"log"
	"net/http"
	"verificacao-cms/config"
	"verificacao-cms/mapper"
)

func CmsSignatureAttachedVerify() {
	params := map[string]string {
		"nonce": config.SignatureNonce,
		"contentReturn": config.ContentReturn,
		"signatures[0][nonce]": config.SignatureNonce,
	}
	files := map[string]string {
		"signatures[0][content]": config.SignatureAttachedPath,
	}
	req, err := NewFormDataRequest(config.UrlCmsVerification, params, files)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", config.AccessToken))

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	verificationReport(resp)
}

func CmsSignatureDetachedVerifySendingDocument() {
	params := map[string]string {
		"nonce": config.SignatureNonce,
		"contentReturn": config.ContentReturn,
		"signatures[0][nonce]": config.SignatureNonce,
	}
	files := map[string]string {
		"signatures[0][content]": config.SignatureDetachedPath,
		"signatures[0][documentContent]": config.DocumentPath,
	}
	req, err := NewFormDataRequest(config.UrlCmsVerification, params, files)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", config.AccessToken))

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	verificationReport(resp)
}

func CmsSignatureDetachedVerifySendingHash() {
	hexEncoded := GetHexadecimalHashFile(config.DocumentPath)

	params := map[string]string {
		"nonce": config.SignatureNonce,
		"contentReturn": config.ContentReturn,
		"signatures[0][nonce]": config.SignatureNonce,
		"signatures[0][documentHashes][0]": hexEncoded,
	}
	files := map[string]string {
		"signatures[0][content]": config.SignatureDetachedPath,
	}
	req, err := NewFormDataRequest(config.UrlCmsVerification, params, files)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", config.AccessToken))

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	verificationReport(resp)
}

func verificationReport(response *http.Response){
	if response.StatusCode == 200 {
		xmlVerifyResponse, err := mapper.JsonMapper{}.DecodeToCmsVerifyResponse(response)
		if err != nil {
			log.Fatal(err)
		}
		log.Println(xmlVerifyResponse.Report())
	} else {
		log.Fatal(mapper.JsonMapper{}.DecodeToString(response))
	}
}
