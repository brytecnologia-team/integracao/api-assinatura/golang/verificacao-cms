package mapper

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"verificacao-cms/model"
)

type JsonMapper struct {}

func (jsonMapper JsonMapper) DecodeToString(response *http.Response) (string, error){
	bytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "", err
	}
	return string(bytes), nil
}

func (jsonMapper JsonMapper) DecodeToCmsVerifyResponse(response *http.Response) (*model.CmsVerifyResponse, error) {
	bytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	var xmlVerifyResponse model.CmsVerifyResponse
	err = json.Unmarshal(bytes, &xmlVerifyResponse)
	return &xmlVerifyResponse, err
}
